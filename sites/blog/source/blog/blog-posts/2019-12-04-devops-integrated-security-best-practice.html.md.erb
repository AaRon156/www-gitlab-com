---
title: "Up your DevOps game with integrated security practices"
author: Vanessa Wegner
author_gitlab: vwegner
author_twitter: gitlabvanessa
categories: insights
image_title: "/images/blogimages/devops-integrate-security.jpg"
description: "Three best practices to bring your projects from DevOps to DevSecOps."
tags: collaboration, DevOps, security, testing
ee_cta: false #
install_cta: false #
twitter_text: "3 best practices for security and #DevOps collaboration" 
postType: content marketing
merch_banner: merch_three
merch_sidebar: merch_three
featured: 
---


In recent years, apps have become our constant companions for both work and play. 
They do some really cool stuff - like alerting you to the fact that you haven’t 
moved in four hours, teaching you how to cook pasta in the most efficient (read: 
lazy) way possible, and showing you so, so many pictures of cute cats.

They also collect data. Tons of it. They connect directly to your network, feed 
information into your business decisions and help you create a competitive 
advantage. But what happens when that app isn’t well-protected? Chaos. 
Devastation. So many angry tweets. 

Aside from the social media rampage, a successful cyberattack can wreak havoc 
on your business, especially if it goes undiscovered for weeks or months. 
Possible fines, lawsuits, lost customers, executive turnover, and general 
turmoil await the next victim of a massive attack. 

For these reasons, you need to take care of application security, best done by 
integrating it into your software development lifecycle at the earliest possible 
phase. 

## Security can’t come in last anymore

Say you’re at an amusement park. It’s awesome. You’re about to ride a brand new 
roller coaster, and you’re pumped. Would you want to wait in line for three hours, 
only to be turned away because you wore the wrong type of shoes? If only you 
had known the ride required trainers instead of Birkenstocks. 

This is how application security is traditionally managed. Your shoes are the 
security criteria. The roller coaster is an application launch. And the line is 
your development pipeline. Why wait until the end of the dev process to find out 
which security requirements you need to fulfill? 

Web applications are among the top two attack vectors, and yet [according to 
Gartner](https://www.gartner.com/document/3893777?ref=solrAll&refval=231221096&qid=b1a1c4b9b5abdb1fdeac1dee), 
application security sees the least amount of spending from IT security budgets 
across many industries - ringing in at only 14% on average. 

<%= partial "includes/blog/blog-merch-sidebar-dynamic" %>

## Secure sooner, release faster

Application security is not just about running tests at every code commit 
(although you should definitely do that); a strong DevSecOps practice will build 
security into the design phase, before any code is written. Improve your 
lifecycle efficiency with these three best practices:

### 1. Good application security relies on trust and collaboration

Collaboration needs to start immediately on every project: Bring together your 
project leaders and security delegates. Give them one meeting (with a pre-read 
or pre-plan) to devise a set of security measures that need to be fulfilled by 
code written for this project, and plan out the automated tests that developers 
will need to run on their code. Making these decisions together will foster both 
trust in the process and encourage buy-in to a security-by-design mentality. 

[Rob Cuddy from IBM advises](https://securityintelligence.com/take-your-relationship-with-devsecops-to-the-next-level/) 
your joint team adopt three important communication points to take your DevSecOps 
to the next level:
1. Communicate only serious issues - and filter out excess noise using AI and machine learning to verify your security scans.
1. Talk about the elephant in the room: Open source. Third party and open source code is omnipresent in software development, so it’s critical to address it directly to reduce the likelihood of preventable attacks.
1. Get to the root issues and deal with them faster: Find and fix false negatives before they’re exploited.

Take these steps to encourage direct, honest, and tactful communication across 
teams - it will help you build and maintain a level of trust and credibility 
critical to efficient and effective DevSecOps.

### 2. Follow through on security planning with integrated testing

Given the limited resources available for application security and its 
criticality for business success, it only makes sense to run tests at every 
code commit. Ideally, these tests will be written once to meet project or 
organizational standards, and then run automatically against every code change. 
[Gartner advises](https://www.gartner.com/document/code/346925?ref=authbody&refval=3889011) 
teams focus tests on areas within the application that provide the most coverage 
but require minimal maintenance. [Also according to Gartner](https://www.gartner.com/document/3889011?ref=gfeed), 
teams should analyze code from every structural level to look for issues 
affecting the operational performance of an app. The code should be safe, 
robust, efficient, and easy to maintain.

Preventative measures like [SAST](https://docs.gitlab.com/ee/user/application_security/sast/) 
and [dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) 
will save time in later phases by reducing the number of code defects before the 
code is merged and helping developers understand how changes will impact other 
areas of the application. Establishing test criteria first will also help 
developers improve the overall quality of their code by providing them with 
standards to refer to and achieve while writing code.

### 3. Use test results to educate, not punish

Applying test results as negative reinforcement is not a constructive practice. 
Beyond remediation, results can be leveraged in two ways:
1. The individual developer should use results as learnings on how to produce higher quality code.
1. At the group level, test results should be scanned for patterns in coding practices that can be improved upon, and used to create standards that will help improve code quality across the entire team or organization. 

## Improve application security and time to delivery with DevSecOps

The general consensus is in: Time spent up-front on security saves significantly 
more time and resources in the final phases of app development. As businesses 
race to keep up with customer expectations, they also must keep pace with an 
evolving cybersecurity landscape - an effort that often starts with the developer: 
The quality of an app’s code is its last line of defense.



Cover image by [Denys Nevozhai](https://unsplash.com/@dnevozhai?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/photos/7nrsVjvALnA)
{: .note}

