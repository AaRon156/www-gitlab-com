---
layout: markdown_page
title: "Category Direction - Runbooks"
---

- TOC
{:toc}

## Runbooks


Runbooks are a collection of documented procedures that explain how to carry out a particular process, be it starting, stopping, debugging, or troubleshooting a particular system.

Historically, runbooks took the form of a decision tree or a detailed step-by-step guide depending on the condition or system.

Modern implementations have introduced the concept of an “executable runbooks”, where, along with a well-defined process, operators can execute pre-written code blocks or database queries against a given environment.

We aim to extend Release Management and Incident Management further by rendering runbooks inside GitLab as interactive documents for operators. This will link from either a release or an incident management screen so when an action happens, GitLab points you to the relevant runbook. Additionally, we want to allow runbooks to trigger ChatOps functions as defined in gitlab-ci.yml.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=runbooks)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/380) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's next & why

Currently, we are taking the first steps of Runbooks within the [Release](https://about.gitlab.com/handbook/engineering/development/ci-cd/release/) stage under the [Release Management Group](https://about.gitlab.com/handbook/product/categories/#release-management-group). We aim to first create visibility to our user's runbooks by enabling an asset link to a runbook via [gitlab#9427](https://gitlab.com/gitlab-org/gitlab/-/issues/9427). In the next iteration, we plan to render a release progress view calculated from the linked runbook in [gitlab#](https://gitlab.com/gitlab-org/gitlab/issues/207258).

## Competitive landscape

While there are some offerings in the marketplace, they rely on heavy customization for every use case and are somewhat specialized (i.e. networking). 

Some of the main competitors for release mangement include [deployment plans](https://docs.xebialabs.com/v.9.5/xl-deploy/concept/examples-of-orchestrators-in-xl-deploy/#default-orchestrator) within XebiaLabs, release orchestration plans in [Electric Cloud's Flow](https://www.cloudbees.com/products/flow/overview), and even layers of spredsheets that are used to track tasks manually. On the Indicident Management side, we see the top providers for runbooks or playbooks are [PagerDuty](), [VictorOps](https://victorops.com/blog/runbooks-checklist), and [OpsGenie](https://www.atlassian.com/software/opsgenie/features). The largest competitor of interest in this space would be the OpsGenie offering, as it is an Atlassian product, it can leverage a resemblance to a DevOps Platform suite. 

## Analyst landscape

Runbook Automation (RBA) is a common terminology used in conjunction with the evaluation of monitoring and incident mangement solutions. Oftentimes, these are not evaluated exclusively by analysts as core offerings. 

## Top Customer Success/Sales issue(s)

We have received several interested customer accounts and prospects in being able connect their own runbooks to release artifacts as described in [gitlab#9427](https://gitlab.com/gitlab-org/gitlab/-/issues/9427).

## Top user issue(s)

[Group-level filtering for JupyterHub deployment](https://gitlab.com/gitlab-org/gitlab-ce/issues/52536)

## Top internal customer issue(s)

Many GitLab teammates have cited an interest in enabling the execution of markdown as code, which can be seen in [gitlab#198287](https://gitlab.com/gitlab-org/gitlab/-/issues/198287), given that many steps or current runbooks maybe stored as markdown files in the current state. Another popular is to make it easier to use runbook style operational code snippets via [gitlab#36386](https://gitlab.com/gitlab-org/gitlab/-/issues/36386). Both of these issues improve the operational effectiveness surrounding native GitLab runbook capabilties. 

## Top Vision Item(s) 

A keystone of runbooks and making they highly useable can center around building out more comprehensive [Runbook Automation](https://gitlab.com/gitlab-org/gitlab-ee/issues/3911) to support all parties that use runbooks whether for Release Management, Incident Response, configuration of infrastructure or even Alerting. In the Release Management space, we are working on building our more native GitLab runbooks as seen in [gitlab&2665](https://gitlab.com/groups/gitlab-org/-/epics/2665). Jupyter Hub offers compelling functionality in their notebooks, so we are also interested in expanding the Jupyter Notebook support for releases via [gitlab&2663](https://gitlab.com/groups/gitlab-org/-/epics/2663). One way to support the execution of these scripts could be to leverage and partner with a provider like [Rundeck](https://www.rundeck.com/open-source), which is being considered in [gitlab#36655](https://gitlab.com/gitlab-org/gitlab/-/issues/36655).
