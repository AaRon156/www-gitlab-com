---
layout: markdown_page
title: "Strategic Marketing Project Management Overview"
---

## On this page
{:.no_toc}

- TOC
{:toc}



### Project management overview
In Strategic Marketing, we have several processes to manage the work the team does

1. [Commitment management](https://about.gitlab.com/handbook/marketing/product-marketing/getting-started/sm-project-management/index.html#commitment-management) - how do we know what we're committed to
1. [Epics (larger projects)](https://about.gitlab.com/handbook/marketing/product-marketing/getting-started/sm-project-management/index.html#epics-and-milestones---planning-and-tracking-our-work) - how do we plan big projects
1. [Monitoring and reporting progress](https://about.gitlab.com/handbook/marketing/product-marketing/getting-started/sm-project-management/index.html#metrics-and-kpis-gitlab-insights) - how do we track progress
1. [Using Labels and keeping them clean](https://about.gitlab.com/handbook/marketing/product-marketing/getting-started/sm-project-management/index.html#labels-and-label-hygiene) - how do we keep our labels
1. [Priority](https://about.gitlab.com/handbook/marketing/product-marketing/getting-started/sm-project-management/index.html#priority-and-prioritization) - how do we indicate what's most important

### Commitment management

We are often asked / requested to work on multiple efforts, across the company.  For example,
- An event, needs booth messaging
- A campaign, needs positioning/messaging and perhaps a gated white-paper
- A campaign, needs a customer case study
- A team wants an analyst inquiry

Either way, a real challenge is how to manage and track our commitments to support other workstreams.

We've established the following  workflow/process in order for us to consistently capture **requests** and manage our **commitments**.   

**The bottom line:**  If we don't have a SM_Request Issue that captures our **Commitment**, then it's not visible.

<iframe width="560" height="315" src="https://www.youtube.com/embed/cuIHNintg1o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### SM Request Process

![SM Request Flow](/handbook/marketing/product-marketing/images/SM_Request_FLow_V3.png){: .margin-right20 .margin-left20 .margin-top20 .margin-bottom20 .image-width50pct }

The process is simple:
1. [Open an SM Support Request Issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new?issuable_template=A-SM-Support-Request) this link will the *A-SM-Support-Request* template.
1. The Strategic Marketing leadership team will review the request(Daily), assign it to the ideal SM Team, prioritize the work and plan how to support your requests.

![sm_reqest board]((/handbook/marketing/product-marketing/images/SM_Request_FLow_V3.png){: .margin-right20 .margin-left20 .margin-top20 .margin-bottom20 .image-width50pct })

**Strategic marketing request review and assignment flow** (note: the label `sm_request` indicates a request for Strategic Marketing support)  
1. **New requests** start with the label `sm_req::new_request`.
   1. **Triaged** to one of the Strategic Marketing teams (PMM,TMM, Competitive Intel, Partner Marketing, Market Research/Cust Insight) `sm_req::triage` and the team label (`pmm`,`tech-pmm`,`Competitive Intelligence`, `Partner Marketing`, `mrci`) who will determine if if there is enough detail to prioritize and plan the work - is it clear?   Then the issue will be route to either:
   1. **Backlog** `sm_req::backlog` for future scheduling, sequencing, and implementation.  *note: add issue to the SM_Backlog milestone for tracking.* **NOTE: Issues in the backlog are NOT yet committed to be done!**
   1. **Assigned** `sm_req::assigned` to team members.   When an issue is assigned, it is added to the quarter **milestone** so we can track status of all the work in flight. **NOTE: Assigned issues should be considered committed to be done!**
   1. *Transferred* `sm_req::transferred` for requests that belong in a different team (Field Marketing, Sales, Ops, etc). Once an issue is transferred, it should be **closed**
   1. *declined* `sm_req::declined` - when an issue is in the backlog and it is no longer relevant or does not make sense anymore.  **Close** the issue when you *decline* it.
1. When **complete**, the team member will update the issue with `sm_req::completed` and **Close** the issue


### Epics and Milestones - planning and tracking our work

#### Regular Work

- Milestones for each quarter to track progress of 'assigned' work

For example, in order to visualize all our regular work in a given quarter, we have a "Quarter Milestone" that makes it possible to visualize and summarize all the work within a given quarter.   This is an experiment to decide how to best use milestones in our regular work.   In the near future (12.10 or 13.0), GitLab will support assigning **Multiple** milestones to a given issue, which will open the door to both managing real "Sprints", and other topics through the Milestone feature in Gitlab.  (Today - the limit is 1 Milestone per Issue)


####  Complex projects

On a regular basis, we typically allocate regular work into a milestone, (defined When we've had larger efforts to manage (multiple tasks, multiple teams, inter-related work, etc).

- Epics  - Define major initiatives with inter-related work streams
- Sub Epics - Sub work streams for different components of a major initiative
- Issues - Specific deliverables / outputs
- Milestones - define time windows to track completion of deliverables.

For example the UseCase GTM Project to build out the messaging, demos, comparisons, case studies and proof points for the Use Cases.   Here specific sub issues are created and associated with the correct epic.

<iframe width="560" height="315" src="https://www.youtube.com/embed/D74xKFNw8vg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- An Overall Epic - [UseCase GTM Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/654)
- and then child Epics for each UseCase - for example:
  - [SCM UseCase Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/655)
  - [CI UseCase Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/661)
- And then we broke each epic into Months so we could see the associated
   - [SCM Month 1 Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/665)

![epic](/handbook/marketing/product-marketing/images/epic.png){: .margin-right20 .margin-left20 .margin-top20 .margin-bottom20 .image-width50pct }

We've organized our work by month, and have a Monthly "Sprint"/Milestone that helps us track completion of the issues/deliverables.

For example this "Milestone" - shows a summary of ALL the usecase work in the Month of April.

![milestone](/handbook/marketing/product-marketing/images/milestone.png){: .margin-right20 .margin-left20 .margin-top20 .margin-bottom20 .image-width50pct }

[UseCase 2020-3](https://gitlab.com/gitlab-com/marketing/product-marketing/-/milestones/13)


### Metrics and KPIs (GitLab Insights)

We are experimenting how to utilize [GitLab Insights](https://docs.gitlab.com/ee/user/project/insights/)

This enables key views such as:

Number of SM Request Issues by Month

![sm-request-insights](/handbook/marketing/product-marketing/images/sm-request-insights.png)

or

Look at the type of PMM work (internal vs external) by Month

![pmm insights](/handbook/marketing/product-marketing/images/pmm-insights.png)

[GitLab Strategic Marketing Insights](https://gitlab.com/gitlab-com/marketing/product-marketing/insights/#/smCharts)

<iframe width="560" height="315" src="https://www.youtube.com/embed/OMTfPsLa98I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Labels and Label Hygiene

We have adopted the Gitlab Triage bot as a way to establish clear policies for labels and issue hygiene.  This allows us to create a set of process *rules* and *policies* and then automatically apply them to our issues.   This helps us to keep issues in the expected state with the expected labels.

See this summary of how to set up and use the [Gitlab Triage Bot](/handbook/marketing/product-marketing/getting-started/105)

<iframe width="560" height="315" src="https://www.youtube.com/embed/Tp79e5sgpao?start=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Priority and Prioritization

At this point, we have three "Priority" labels in Strategic Marketing.   These are both `scoped` labels (so you can't have both `P::1` and `P::2`) at the same time.  The labels are also defined as "Priority", so they will sort issues where they are assigned.

1. `P::1`
1. `P::2`
1. `P::3`

Over time, we will be establishing guidelines about how we consistently use these labels to communicate priority within the team.  
