---
layout: job_family_page
title: "Digital Production"
---

## Digital Production, Producer

### Responsibilities

- Liaise with internal teams to assist with pre- and post-production for GitLab branded audio and video projects and events, including but not limited to Contribute, Connect events, Commit user conference, Technical Evangelism, Community Evangelism, Employment branding, sales enablement, social, and livestreams.
- Assist in the planning and coordination of productions, including but not limited to scouting locations, talent, and crew.
- Support integrated marketing campaigns, events, and initiatives via audio and video projects.
- Write and/or edit treatments and scripts for multimedia packages.
- Pitch ideas for customer and user stories as well as promotional videos.
- Assist with the management of GitLab YouTube channels.

### Requirements

- Bachelor’s Degree or extensive training in broadcast journalism, film & television, or audio/video production
- 3+ years of production experience and at least 1 year of marketing experience
- Must be a creative self-starter with exceptional multimedia storytelling skills
- Excellent writing and communication skills
- Highly collaborative. This role will require effective collaboration across multiple teams, organizations, and individuals.
- Extremely detail-oriented and organized
- Basic video editing skills
- Podcasting experience a plus
- Frequent travel required
- You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values
* Ability to use GitLab
- BONUS: A passion and strong understanding of the industry and our mission

## Digital Production Manager

### Responsibilities

- Develop multimedia (audio/video) content for brand and marketing initiatives.
- Plan and manage production for projects; write treatments, determine scope, scout and select locations and talent, coordinate shoots.
- Manage creative projects, including scripting, storyboarding, and graphic elements, to ensure brand consistency.
- Direct video shoots and manage production crew.
- Collaborate closely with internal clients and stakeholders to assess and execute production needs.
- Execute video & audio production for GitLab corporate events.
- Manage video library.

## Requirements
- 5+ years of experience in video and audio production
- Excellent communication, storytelling, and presentation of skills
- Strong organizational skills, ability to handle projects end-to-end
- Proficiency with video and audio software and equipment
- You share our values, and work in accordance with those values.
- BONUS: A passion and strong understanding of the industry and our mission.


## Manager, Digital Production

## Responsibilities

- Manage digital production team.
- Develop multimedia (audio/video) strategy for brand and marketing initiatives.
- Set and manage the quarterly and bi-annual production deliverables and schedule.
- Manage digital production budget, vendors, schedules, and equipment.
- Plan and manage production; write treatments, determine scope, scout and select locations and talent, coordinate shoots.
- Manage creative, including scripting, storyboarding, and graphic elements, to ensure brand consistency.
- Direct video shoots and manage production crew.
- Collaborate closely with internal clients and stakeholders to assess and prioritize production needs.
- Manage audio and video production for GitLab corporate events.
- Report on production metrics.

## Requirements

- 5+ years of experience in video and audio production
- Previous management experience
- Proven ability to manage production budgets and timelines
- Excellent communication, storytelling, and presentation of skills
- Strong organizational skills, ability to handle projects end-to-end
- Proficiency with video and audio software and equipment
- You share our [values](/handbook/values), and work in accordance with those values.
- BONUS: A passion and strong understanding of the industry and our mission.
